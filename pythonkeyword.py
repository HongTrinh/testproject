from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#driver = webdriver.Chrome()
#driver.get(" http://www.demo.guru99.com/v4")
#assert "Python" in driver.title
#elem = driver.find_element_by_name("q")
#elem.clear()
#elem.send_keys("pycon")
#elem.send_keys(Keys.RETURN)
#assert "No results found." not in driver.page_source
#driver.close()


def OpenBrowser(url):
    driver = webdriver.Chrome()
    driver.get(url)
    return driver

def Login(driver, xpath1, username, xpath2, password):
    driver.find_element_by_xpath('//input[@name="uid"]').click()
    driver.find_element_by_xpath('//input[@name="password"]').click()
    driver.find_element_by_xpath(xpath1).send_keys(username)
    driver.find_element_by_xpath(xpath2).send_keys(password)
    driver.find_element_by_xpath('//input[@name="btnReset"]').click()
    driver.find_element_by_xpath(xpath1).send_keys(username)
    driver.find_element_by_xpath(xpath2).send_keys(password)
    driver.find_element_by_xpath('//input[@name="btnLogin"]').click()

