tc2
    [Documentation]    Exercise 1: Verify the Login Section
    ...     \ \ ... \ \ \ - Go to http://demo.guru99.com/v4
    ...     \ ... \ \ \ - Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty
    ...     ... \ \ \ - Input userID and password
    ...    ... \ \ \ - Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared
    ...    ... \ \ \ - Click Reset button
    ...    ... \ \ \ - Verify userID and password is cleared
    ...    ... \ \ \ - Input valid userID and valid password
    ...    ... \ \ \ - Click button login
    ...    ... \ \ \ - Verify login to home page successful
    ...    ... \ \ \ - Verify dynamic userID in home page
    ...    ... \ \ \ - Evidence screenshots are required for verification steps
    ${session}    open browser    http://demo.guru99.com/v4
    login    ${session}    //input[@name="uid"]    mngr341477    //input[@name="password"]    UvunEmy