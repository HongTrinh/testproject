*** Settings ***
Library           Selenium2Library

*** Test Cases ***
TC01
    Open Browser    https://demo.guru99.com/v4/    Chrome
    Click Button    //button[@id="details-button"]
    Click Link    //a[@id="proceed-link"]
    Input Text    //input[@type="text"]    mngr336038
    Clear Element Text    //input[@type="text"]
    Get Element Attribute    //input[@type="text"]    attribute=None
    Sleep    7s
    Input Text    //input[@type="password"]    myjArAp
    Clear Element Text    //input[@type="password"]
    Get Element Attribute    //input[@type="password"]    attribute=None
    Sleep    7s
    Element Should Not Contain    //label[@id="message23"]    expected
    Input Text    //input[@type="text"]    mngr336038
    Input Password    //input[@type="password"]    myjArAp
    Click Button    //input[@value="RESET"]
    ${value}    Get Text    //input[@type="text"]
    Should Be Equal    ${value}    ${EMPTY}
    ${value1}    Get Text    //input[@type="password"]
    Should Be Equal    ${value1}    ${EMPTY}
    Sleep    7s
    Input Text    //input[@type="text"]    mngr334878
    Input Password    //input[@type="password"]    EnUpApU
    Click button    //input[@value="LOGIN"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php

TC02
    [Documentation]    Exercise 2: Verify add new customer
    ...    - Login home page
    ...    - select new customer
    ...    - Fill all fields
    ...    - submit form
    ...    - Verify add new customer's information correct with value input above
    ...    - Logout
    ...    - Verify login page displays
    ...    - Evidence screenshots are required for verification steps
    [Tags]    TC02
    Open browser    https://demo.guru99.com/v4/    Chrome
    Click Button    //button[@id="details-button"]
    Click Link    //a[@id="proceed-link"]
    Input Text    //input[@type="text"]    mngr336038
    Input Password    //input[@type="password"]    myjArAp
    Click button    //input[@value="LOGIN"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    Click Element    //a[text()="New Customer"]
    Input Text    //input[@name="name"]    PhamThiHongTrinh
    Click Button    //input[@value="f"]
    Input Text    //input[@name="dob"]    20/08/2000
    Input Text    //textarea[@name="addr"]    70 Nguyen Du
    Input Text    //input[@name="city"]    DaNang City
    Input Text    //input[@name="state"]    Da Nang
    Input Text    //input[@name="pinno"]    123456
    Input Text    //input[@name="telephoneno"]    0896204058
    Input Text    //input[@name="emailid"]    hongtrinh18@gmail.com
    Input Text    //input[@name="password"]    Trinh2008
    #step3
    Click Button    //input[@value="Submit"]
    #step4
    Click Element    //a[text()="Continue"]
    Location should be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    Click Element    //a[text()="Log out"]
    Location Should Be    https://demo.guru99.com/v4/index.php

TC03
    [Documentation]    Exercise 3: Verify sort order using xpath Axes method
    ...    - Go to URL: http://live.guru99.com
    ...    - Click on Mobile menu
    ...    - Verify sort by position
    ...    1. Sony Experia
    ...    2. IPHONE
    ...    3. SAMSUNG GALAXY
    ...    - Click sort by Name
    ...    - Verify sort by name
    ...    1. IPHONE
    ...    2. SAMSUNG GALAXY
    ...    3. Sony Experia
    ...    - Click sort by Price
    ...    - Verify sort by Price
    ...    1. Sony Experia
    ...    2. SAMSUNG GALAXY
    ...    3. IPHONE
    [Tags]    TC03
    Open browser    http://live.guru99.com    Chrome
    Click Element    //a[text()="Mobile"]
    Click Element    //select[@onchange="setLocation(this.value)"]//child::option
    Click Element     //select[@title='Sort By']/option[1]
    Sleep    4s
    Click Element     //select[@title='Sort By']/option[2]
    Sleep    4s
    Click Element     //select[@title='Sort By']/option[3]
